package com.asesoria.repositories;

import com.asesoria.models.AsesoriaModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AsesoriaRepository extends JpaRepository<AsesoriaModel, Long> {
}
