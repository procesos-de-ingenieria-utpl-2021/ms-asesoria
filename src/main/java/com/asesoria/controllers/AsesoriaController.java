package com.asesoria.controllers;

import java.util.Random;

import javax.validation.Valid;

import com.asesoria.exception.ResourceNotFoundException;
import com.asesoria.models.AsesoriaModel;
import com.asesoria.repositories.AsesoriaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AsesoriaController {

    @Autowired
    private AsesoriaRepository asesoriaRepository;

    @GetMapping("/asesoria")
    public Page<AsesoriaModel> getAsesoria(Pageable pageable) {
        return asesoriaRepository.findAll(pageable);
    }

    @PostMapping("/asesoria")
    public AsesoriaModel createAsesoria() {
        AsesoriaModel asesoria = new AsesoriaModel();
        asesoria.setRecommendation("recommendation" + new Random().nextInt());
        return asesoriaRepository.save(asesoria);
    }

    @PutMapping("/asesoria/{asesoriaId}")
    public AsesoriaModel updateQuestion(@PathVariable Long asesoriaId,
            @Valid @RequestBody AsesoriaModel questionRequest) {
        return asesoriaRepository.findById(asesoriaId).map(question -> {
            question.setRecommendation(questionRequest.getRecommendation());
            return asesoriaRepository.save(question);
        }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + asesoriaId));
    }

    @DeleteMapping("/asesoria/{questionId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long asesoriaId) {
        return asesoriaRepository.findById(asesoriaId).map(question -> {
            asesoriaRepository.delete(question);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Asesoria not found with id " + asesoriaId));
    }
}
